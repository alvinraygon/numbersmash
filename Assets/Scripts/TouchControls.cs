﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TouchControls : MonoBehaviour {
	
	private musicTriggers mscTrigger;
	public Sprite spr;
	public int pointsToAdd;
	public int counterToAdd;
	public int toBeDeducted;
	private IEnumerator cor;
	public int didHit = 0;
	public AudioClip[] sfxClip;


	void Start(){
//		cor = doTransitionOfSprite;
		mscTrigger = GameObject.Find("SoundManager").GetComponent<musicTriggers>();
	}
	void Update(){
		mscTrigger = GameObject.Find("SoundManager").GetComponent<musicTriggers>();
	}

	public void OnMouseOver()
	{

	if (Input.GetMouseButtonDown (0)) {
			if (didHit == 0) {
				didHit = 1;

				if (gameObject.tag == "Right") {
					mscTrigger.PlaySingle (sfxClip[0]);
					ScoreManager.AddPoints (pointsToAdd);
					this.gameObject.GetComponent<Animator> ().enabled = false;
					this.gameObject.GetComponent<SpriteRenderer> ().sprite = spr;
					Counter.AddCounter (counterToAdd);
					StartCoroutine (doTransitionOfSprite ());


				} else if (gameObject.tag == "Wrong") {
					mscTrigger.PlaySingle (sfxClip[1]);
					ScoreManager.AddPoints (pointsToAdd);
					this.gameObject.GetComponent<Animator> ().enabled = false;
					this.gameObject.GetComponent<SpriteRenderer> ().sprite = spr;
					HeartAndStars.MinusHeartAndStars (toBeDeducted);
					StartCoroutine (doTransitionOfSprite ());
				}
			} else {
				//do nothing
			}
		} else {
		//do nothing
		}//

	}
	IEnumerator doTransitionOfSprite(){
		yield return new WaitForSeconds (0.4f);
		Destroy (gameObject);
	}
}
