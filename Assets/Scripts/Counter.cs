﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Counter : MonoBehaviour {
	
	public Slider CounterBar;
	public int OldCounter;
	public static int NewCounter;
	public GameObject LevelSuccess;
	public GameObject Right;
	public GameObject Wrong;
	public GameObject UpperFrame;
	public GameObject Slider;
	public GameObject Pause;
	public GameNumbers gameNumbers;
	public bool didResume,didRestart;
	public int finishWithStars;

	void Awake(){
		finishWithStars = 0;
		gameNumbers = GameObject.Find ("GameManager").GetComponent<GameNumbers>();

	}

	void Start () {
		CounterBar = GetComponent<Slider> ();
	}


	void Update(){

			CounterBar.value = OldCounter;
			OldCounter = NewCounter;

		if(OldCounter == 12 || OldCounter>=12){
			NewCounter = 0;
			LevelSuccess.SetActive (true);
			Right.SetActive (false);
			Wrong.SetActive (false);
			UpperFrame.SetActive (false);
			Slider.SetActive (false);
			Pause.SetActive (false);
			finishWithStars = LevelSuccess.GetComponentInChildren<StarCountClass> ().CountActive ();
			Debug.Log ("you get " +finishWithStars+" stars!");
			LevelSuccess.GetComponent<Animator> ().enabled = true;
		}

	}

	public static void AddCounter(int counterToAdd)
	{
		NewCounter += counterToAdd;

	}

	public void ResumeCounter(){
		didResume = true;
	}
	public void RestartCounter(){
		didRestart = true;
	}

}
